#!/bin/bash
# ELDK/Yocto sets up some funny Python stuff, 
# which keeps my test cases from running
# This is a work around which seems to work
HERE=$(pwd)
(
export DEVKIT="armv7a"
source ../../../env.sh
source ../../../../../common-test-scripts/common-tests.sh

cd ${HERE}

HERE_MINUS_TWO="$(echo ${HERE} | sed -r 's|/home/student/||')"

if [ "${TRAINING_ID_SHORT}" = "yocto" ]; then
   export TESTARGS="-c $YOCTO_IMAGE_5_CHOICE -p $HERE_MINUS_TWO"
fi
#echo $TESTARGS

pushd ../
INSTALL_PATH=${PWD##*/}
TESTCASE=${PWD##*/}
popd
if [ "${TRAINING_ID_SHORT}" = "yocto" ]; then
   export TARGET_TESTARGS="-ip ${BOARDIP} -p ${HOME}/${INSTALL_PATH} -rpd ${REMOTE_POWER_DEVICE} -cd ${CONSOLE_DEVICE}"
else #intely?
   export TARGET_TESTARGS="-ip ${BOARDIP}"
fi
echo $TARGET_TESTARGS
#echo $INSTALL_PATH

echo "/workdir/killall_bitbake.sh | ccze -A" > ${HERE}/test.sh

# ===================================================================================================

# TESTARGS, TESTCASE, TARGET_TESTARGS, TESTLOG_PATH are usd in functions:
#in-build-container test_00.06.py
#in-build-container test_00.07.py
#on-host test_00.08.01_on_host.py
#on-host test_00.08.02_on_host.py
#on-target-console test_00.08.03_on-target-console.py
in-build-container test_03.py
#in-build-container test_04.01.py
#on-host test_04.02_on_host.py
#on-target-console test_04.03_on-target-console.py

chmod +x ${HERE}/test.sh

)
cd /workdir
${HERE}/test.sh
rm -f ${HERE}/test.sh

ps -ef | grep python3
ps -ef | grep remote_power
ps -ef | grep console
ps -ef | grep tests

