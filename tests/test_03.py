#!/usr/bin/env python3
import pexpect
import sys
import os
import argparse

try:
  parser = argparse.ArgumentParser()

  #parser.add_argument("-ip" , "--ip_addr", type=str, required=True)
  parser.add_argument("-p"  , "--pwd"    , type=str, required=False)
  parser.add_argument("-c"  , "--cfg"    , type=str, required=True)
  #parser.add_argument("-d", "--integer", type=int, default=50)

  args = parser.parse_args()
  # if we explicitly pass the (target)_pwd:
  if args.pwd:
    #print 'IP_ADDR:' + args.ip_addr
    print('PWD: ' + '/' + args.pwd)
    print('CFG: ' + args.cfg)
  else:
    #print 'IP_ADDR:' + args.ip_addr
    print('CFG: ' + args.cfg)

  docker_pwd = '/' + args.pwd
  print('docker_pwd: ' + docker_pwd)
  docker_pwd_back = docker_pwd + '/../../'
  print('docker_pwd_back: ' + docker_pwd_back)
  rel_pwd= os.path.relpath(docker_pwd, docker_pwd_back)
  print('rel_pwd: ' + rel_pwd)
except IndexError:
  print('Must have cfg and pwd')
  sys.exit(1)

resy_poky_container='/workdir/resy-poky-container.sh ' + args.cfg
#child = pexpect.spawn('/workdir/resy-poky-container.sh multi-v7-ml-debug-training-master', encoding='utf-8')
child = pexpect.spawn(resy_poky_container, encoding='utf-8')
child.logfile = sys.stdout
index = child.expect_exact("$ ")
print(child.before,child.after)
ts_posix=' | while IFS= read -r line; do printf \'[%s] %s\\n\' "$(date \'+%Y-%m-%d %H:%M:%S\')" "$line"; done'
#cmd='bitbake' + ts_posix
#child.sendline(cmd)
#index = child.expect_exact("$ ")
print(child.before,child.after)

working_dir = docker_pwd+'/../'
cmd='cd '+ working_dir
child.sendline (cmd)
index = child.expect_exact("$ ")

cmd='./03_run.sh' + ts_posix
child.sendline (cmd)
print(child.before,child.after)
index = child.expect_exact("#? ")
cmd='1'
child.sendline (cmd)
print(child.before,child.after)

child.expect('03_run.sh done',timeout=200)
print(child.before,child.after)
