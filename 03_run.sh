#!/bin/bash
RECIPE_NAME="use-libpalindrome"
HERE=$(pwd)
source ../../env.sh
source ../../../../common-scripts/common.sh
from_docker

INSTALL_PATH=${PWD##*/}
plus_on
cd ${YOCTO_IMAGE_5_PATH}
plus_off

#plus_echo "source oe-init-build-env"
#source oe-init-build-env

is_bb_avail

is_bb_eq ${YOCTO_IMAGE_5_PATH} ${HERE}

plus_echo "check if recipe ${RECIPE_NAME} exists:"
plus_echo "bitbake -s | grep ${RECIPE_NAME}"
press_enter
bitbake -s | ${GREP} ${RECIPE_NAME}

#plus_echo "clean sstate of ${RECIPE_NAME}:"
#plus_echo "bitbake -c cleansstate ${RECIPE_NAME}"
#press_enter
#bitbake -c cleansstate ${RECIPE_NAME}

plus_echo "bake ${RECIPE_NAME}:"
plus_echo "bitbake ${RECIPE_NAME} --skip-setscene"
press_enter
bitbake ${RECIPE_NAME} --skip-setscene

plus_on
oe-pkgdata-util list-pkgs ${RECIPE_NAME}*
oe-pkgdata-util list-pkg-files ${RECIPE_NAME}
oe-pkgdata-util list-pkg-files ${RECIPE_NAME}-dev
#oe-pkgdata-util list-pkg-files ${RECIPE_NAME}-staticdev
oe-pkgdata-util list-pkg-files ${RECIPE_NAME}-dbg
oe-pkgdata-util list-pkg-files ${RECIPE_NAME}-src
plus_off

plus_echo "search B"
plus_echo 'export $(bitbake -e ${RECIPE_NAME} | grep ^B= | sed 's/\"//g')'
press_enter
export $(bitbake -e ${RECIPE_NAME} | ${GREP} ^B= | sed 's/\"//g')
plus_echo_green "${B}"

plus_echo "strings ${B}/${RECIPE_NAME} | grep \"Hello\""
press_enter
strings ${B}/${RECIPE_NAME} | color_grep Hello

plus_echo "$(basename ${0}) done"

cd ${HERE}
